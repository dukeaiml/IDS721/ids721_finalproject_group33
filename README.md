# IDS 721 - Final Project - Group 27

![pipeline status](https://gitlab.com/dukeaiml/IDS721/ids721_finalproject_group27/badges/main/pipeline.svg)

This project is a web service written in Rust for sentiment analysis and text generation. It utilizes the Axum framework and RustBert library to perform sentiment analysis on input text quickly and efficiently, while also generating text relevant to the sentiment. The service employs PyTorch's LibTorch library for model inference and supports deployment on Kubernetes. Additionally, we have automated the workflow with a CI/CD pipeline.

Here is our web service [Root Link](http://34.75.67.153/) and [Function Link](http://34.75.67.153/sentiment)

## Team Members

Isaac Wang (cw516), Mutian Ling (ml646), Yang Xu (yx248), and Zibin Gao (zg112)

## Features

* Sentiment Analysis: Automatically analyzes the sentiment tendency of input text and returns the analysis result.

* Text Generation: Generates text relevant to the sentiment analyzed, enhancing user experience.

* High Performance: Written in Rust, leveraging its excellent concurrency and performance features, capable of handling large-scale concurrent requests.

* Containerized Deployment: Built with a Dockerfile, allowing easy deployment to Kubernetes clusters for automation and scalability.

* CI/CD Pipeline: Automated workflow for continuous integration and continuous deployment, ensuring efficient development and deployment processes.

## Service Dependencies
```
axum = "0.7.4"
tokio = { version = "1.36.0", features = ["macros", "full"] }
tower = "0.4.13"
tower-http = { version = "0.5.2", features = ["fs"] }
#tracing = { version = "0.1", features = ["log"] }
#tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
# json
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
# s3
aws-config = { version = "1.1.9", features = ["behavior-version-latest"] }
aws-sdk-s3 = "1.21.0"
csv = "1.1"
# openssl
openssl = { version = "0.10", features = ["vendored"] }
# rust-bert
rust-bert = "0.22.0"
# thiserror
thiserror = "1.0"

```

## Open-source rust-bert

This is the open-source model we used:

https://github.com/guillaume-be/rust-bert

* Install Libtorch 2.1.0 from the link below

https://download.pytorch.org/libtorch/cu118/libtorch-cxx11-abi-shared-with-deps-2.0.0%2Bcu118.zip

* Set the Libtorch Environment Variable
```bash
export LIBTORCH=/path/to/libtorch

export LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH
```



## API Endpoints

- **POST /analyze**: Analyze the sentiment of input text.
  - Request body should contain a JSON object with a `text` field.
  - Example: `{ "text": "I love this product!" }`

- **Response**:

  ```json
  {
    "result": {
      "polarity": "positive",
      "positive_percentage": 80.0,
      "negative_percentage": 20.0,
      "analysis_text": "This text is considered sentiment positive because..."
    }
  }

![Alt text](images/function_result.png)

- **Error Message**

  ```json
  { "error": "S3 service error" }

  { "error": "Invalid command" }

  { "error": "Sentiment analysis error" }

  { "error": "AWS S3 Error" }

  { "error": "Internal Error: {0}" }
  ```
![Alt text](images/function_error_msg.png)



### Dockerization

* Create the dockerfile
```dockerfile
# Use an official Rust image
FROM rust:1.77 as builder

RUN apt-get update
RUN apt-get install -y build-essential cmake curl openssl libssl-dev

ARG LIBTORCH_URL=https://download.pytorch.org/libtorch/cu121/libtorch-cxx11-abi-shared-with-deps-2.1.0%2Bcu121.zip

RUN curl -L ${LIBTORCH_URL} -o libtorch.zip && \
    unzip libtorch.zip -d / && \
    rm libtorch.zip

ENV LIBTORCH=/libtorch
ENV LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH

# Create a new empty shell project
RUN USER=root cargo new idsfinal
WORKDIR /idsfinal

# Copy the manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This is a dummy build to get the dependencies cached
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are built, copy your source code
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/ids721_final*
RUN cargo build --release

# test
RUN cargo test -v

# Final stage
FROM debian:bookworm-slim
RUN apt-get update
RUN apt-get install -y build-essential cmake curl openssl libssl-dev
COPY --from=builder /idsfinal/target/release/ids721_final .
COPY --from=builder /libtorch/ /libtorch/
ENV LIBTORCH=/libtorch
ENV LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH
ENV ROCKET_ADDRESS=0.0.0.0
CMD ["./ids721_final"]

```

### Deploy to Kubernetes


1. Create the cluster

![Alt text](images/image-2.png)

2. Create the image and push it to docker hub

```bash
docker build -t your-image-name:tag .

docker tag your-image-name:tag your-docker-username/your-docker-image:tag

docker push your-docker-username/your-docker-image:tag

```
![Alt text](images/image.png)

3. Install gcloud dependencies

https://cloud.google.com/sdk/docs/install?hl=zh-cn#linux

```bash
sudo apt-get install kubectl
```

```bash
sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin
```

4. Log in

```bash
gcloud auth login
```


to the clusters

```bash
gcloud container clusters get-credentials <cluster name> --zone us-central1 --project <project id>
```
5. Deployment

Yaml file :
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
      - name: myapp
        image: liam9907/myapp:latest
        ports:
        - containerPort: 3000

---
apiVersion: v1
kind: Service
metadata:
  name: myapp-service
spec:
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 3000
  selector:
    app: myapp

```

```bash
kubectl apply -f <yaml file>

kubectl rollout restart deployment <your-app>-deployment
```

6. check the status

```bash
kubectl get deployments
```

7. Get external IP to access

```bash
kubectl get svc
```


![Alt text](images/image-5.png)

- After deployed we can see the status in the Kubernetes Engine Workloads
![Alt text](images/deploy_result_root.PNG)
![Alt text](images/deploy_result.PNG)


## CICD


```yml
# This file is a template, and might need editing before it works on your project.
# This is a sample GitLab CI/CD configuration file that should run without any modifications.
# It demonstrates a basic 3 stage CI/CD pipeline. Instead of real tests or scripts,
# it uses echo commands to simulate the pipeline execution.
#
# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.
#
# For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html#stages
#
# You can copy and paste this template into a new `.gitlab-ci.yml` file.
# You should not add this template to an existing `.gitlab-ci.yml` file by using the `include:` keyword.
#
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Getting-Started.gitlab-ci.yml

stages:          # List of stages for jobs, and their order of execution
  - build
  - test
  - deploy

build-job:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - cd ids721_final
    - docker build -t myapp:v1 .



test-job:   # This job runs in the test stage.
  stage: test    # It only starts when the job in the build stage completes successfully.
  script:
    - echo "Running unit tests... This will take about 60 seconds."
    - cd ids721_final
    - cargo test

deploy-job:      # This job runs in the deploy stage.
  stage: deploy
  image: google/cloud-sdk:latest
  script:
    - cd ids721_final
    - echo $GKE_SERVICE_KEY > gcloud-service-key.json

    - gcloud auth activate-service-account --key-file gcloud-service-key.json
    - gcloud container clusters get-credentials ids721-final --zone us-east1 --project calcium-arcadia-421022
    - kubectl apply -f k8s.yaml
    - kubectl rollout restart deployment myapp-deployment
    - kubectl get deployments
    - kubectl get pods -l app=myapp
    - kubectl get svc
  only:
    - main

```

This GitLab CI/CD configuration file defines a pipeline with three stages: build, test, and deploy. Each stage has corresponding jobs that execute scripts or commands.

*  The build-job builds a Docker image named myapp:v1.

In the test stage:

* The test-job executes unit tests for the Rust project.

In the deploy stage:

* The deploy-job deploys the Docker image to a Kubernetes cluster on Google Cloud Platform (GCP).

* It sets up authentication with GCP using the service account key stored in gcloud-service-key.json.

* The Kubernetes cluster is identified by the name ids721-final, located in the us-east1 zone, and belongs to the project calcium-arcadia-421022.

* It applies the Kubernetes manifest defined in k8s.yaml to create or update Kubernetes resources.

Worth to mention :

we need to set $GKE_SERVICE_KEY as the environment variable for cicd. Go the "IAM & Admin" of google cloud to create a new role and set up a key. Download the json file and then paste the content to $GKE_SERVICE_KEY.

![Alt text](images/image-4.png)

## Loadtest
We use Apach JMeter for load testing. Our service reach the limit of 1000QPS.

![loadtest_result](images/loadtest_result.PNG)


## Video
The Demo Video is uploaded on Youtube. [Link](https://www.youtube.com/watch?v=fHV-R3NNO6Q)
