use axum::{response::{IntoResponse, Response, Json}, http::StatusCode, Json as AxumJson};
use serde::{Deserialize, Serialize};
use serde_json::json;
use aws_sdk_s3::Client as S3Client;
use aws_sdk_s3::primitives::ByteStream;
use aws_config::{self, load_defaults, BehaviorVersion};
use csv::{ReaderBuilder, WriterBuilder};
use rust_bert::pipelines::sentiment::{SentimentModel, Sentiment, SentimentPolarity};
use rust_bert::pipelines::text_generation::TextGenerationModel;
use rust_bert::RustBertError;
// use rust_bert::pipelines::common::ModelType;
use std::sync::Arc;
use tokio::sync::Mutex;
use thiserror::Error;
use std::collections::HashMap;

// models initial
static mut SENTIMENT_MODEL: Option<Arc<Mutex<SentimentModel>>> = None;
static mut TEXT_GENERATION_MODEL: Option<Arc<Mutex<TextGenerationModel>>> = None;

pub fn initialize_models() {
    let sentiment_model = tokio::task::block_in_place(|| {
        SentimentModel::new(Default::default()).expect("Failed to load the sentiment model")
    });
    let sentiment_model = Arc::new(Mutex::new(sentiment_model));

    // Assign the sentiment model to the global variable
    unsafe { SENTIMENT_MODEL = Some(sentiment_model); }

    let text_generation_model = tokio::task::block_in_place(|| {
        TextGenerationModel::new(Default::default()).expect("Failed to load the text generate model")
    });
    let text_generation_model = Arc::new(Mutex::new(text_generation_model));

    // Assign the text generate model to the global variable
    unsafe { TEXT_GENERATION_MODEL = Some(text_generation_model); }
}

impl From<RustBertError> for ApiError {
    fn from(err: RustBertError) -> Self {
        ApiError::InternalError(err.to_string()) // Convert RustBertError to ApiError
    }
}

impl From<serde_json::Error> for ApiError {
    fn from(err: serde_json::Error) -> Self {
        ApiError::InternalError(format!("JSON serialization error: {}", err))
    }
}

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Invalid command")]
    InvalidCommand,
    #[error("Sentiment analysis error")]
    SentimentError,
    #[error("AWS S3 Error")]
    S3Error,
    #[error("Internal Error: {0}")]
    InternalError(String),
}

#[derive(Deserialize)]
pub struct InputText {
    pub text: String,
}

// #[derive(Serialize, Deserialize)]
// pub struct Output {
//     pub result: String,
// }

#[derive(Serialize, Deserialize, Debug)]
pub struct SentimentAnalysisResult {
    polarity: String,
    positive_percentage: f64,
    negative_percentage: f64,
    analysis_text: String,
}

#[derive(Serialize, Deserialize)]
pub struct Output {
    pub result: SentimentAnalysisResult,
}

impl IntoResponse for ApiError {
    fn into_response(self) -> Response {
        let error_message = match self {
            ApiError::InvalidCommand => "Invalid command".to_string(),
            ApiError::SentimentError => "Error analyzing sentiment".to_string(),
            ApiError::S3Error => "S3 service error".to_string(),
            ApiError::InternalError(msg) => msg,  // 这里 `msg` 已经是 `String` 类型，不需要转换
        };

        // 创建 JSON 对象作为响应体
        let body = Json(json!({ "error": error_message }));

        // 构建响应，状态码为 500，直接使用 body
        Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .header("Content-Type", "application/json")
            .body(body.into_response().into_body())  // 直接获取 Json 序列化后的 body
            .unwrap()  // 注意：实际生产代码中应更严谨地处理错误
    }
}

// pub async fn function_handler(input_text: Json<InputText>) -> impl IntoResponse {
pub async fn function_handler(input_text: Json<InputText>) -> Result<Json<Output>, ApiError> {
    let sentiment_model = unsafe {
        SENTIMENT_MODEL.as_ref().unwrap().clone()
    };

    let sentiment_result = analyze_sentiment_and_update_s3(&input_text.text, "ids-721-final-sentiment", sentiment_model).await?;

    Ok(AxumJson(Output { result: sentiment_result }))

    // let json_result = serde_json::to_string(&sentiment_result)
    //     .map_err(|err| ApiError::from(err))?;  // Convert serde_json::Error to ApiError

    // Ok(AxumJson(Output { result: json_result }))
}

async fn analyze_sentiment_and_update_s3(text: &str, bucket: &str, sentiment_model: Arc<Mutex<SentimentModel>>) -> Result<SentimentAnalysisResult, ApiError> {
    // 获取Mutex的锁
    let model = sentiment_model.lock().await;
    let sentiments = model.predict(&[text]);
    let sentiment = sentiments.into_iter().next().ok_or(ApiError::SentimentError)?;

    let config = load_defaults(BehaviorVersion::latest()).await;
    let s3_client = S3Client::new(&config);
    let (positive_percentage, negative_percentage) = update_sentiment_count_in_s3(&s3_client, bucket, &sentiment).await?;

    let analysis_text = generate_sentiment_analyze(&text, &sentiment).await?;

    Ok(SentimentAnalysisResult {
        polarity: format!("{:?}", sentiment.polarity),
        positive_percentage,
        negative_percentage,
        analysis_text,
    })
}

#[derive(Serialize, Deserialize, Debug)]
struct SentimentRecord {
    #[serde(rename = "Sentiment")]
    sentiment: String,
    #[serde(rename = "Count")]
    count: usize,
}

async fn read_and_parse_csv(client: &S3Client, bucket: &str, key: &str) -> Result<Vec<SentimentRecord>, ApiError> {
    let get_req = client.get_object()
                        .bucket(bucket)
                        .key(key)
                        .send()
                        .await
                        .map_err(|_| ApiError::S3Error)?;

    let bytes_stream = get_req
        .body
        .collect()
        .await
        .map_err(|_| ApiError::S3Error)?;

    let bytes = bytes_stream.into_bytes();

    let csv_content = std::str::from_utf8(&bytes)
        .map_err(|_| ApiError::InternalError("Invalid UTF-8 sequence".into()))?;

    let mut rdr = ReaderBuilder::new().from_reader(csv_content.as_bytes());
    let records: Result<Vec<SentimentRecord>, csv::Error> = rdr.deserialize().collect();
    records.map_err(|_| ApiError::InternalError("Failed to parse CSV".into()))
}

async fn update_sentiment_count_in_s3(client: &S3Client, bucket: &str, update_sentiment: &Sentiment) -> Result<(f64, f64), ApiError> {
    let key = "sentiment.csv";
    let records = read_and_parse_csv(client, bucket, key).await?;
    // println!("Read records: {:?}", records);  // check if records are read correctly

    // 更新情感计数
    let mut map = records.into_iter().fold(HashMap::new(), |mut acc, rec| {
        // acc.entry(rec.sentiment).or_insert(rec.count);
        *acc.entry(rec.sentiment).or_insert(0) += rec.count; // 注意这里需要进行累加
        acc
    });
    // 定义更新键的变量
    let key_to_update = format!("{:?}", update_sentiment.polarity);
    // println!("Trying to access key: {:?}", key_to_update);
    // println!("Trying to access key: {:?}", format!("{:?}", update_sentiment.polarity)); // check map key

    *map.entry(key_to_update).or_insert(0) += 1; // 直接使用 entry API 安全更新或插入

    // println!("Updated map: {:?}", map);  // check if map is updated correctly

    // 将更新后的数据写回 CSV
    let mut wtr = Vec::new();

    {
        let mut csv_writer = WriterBuilder::new().from_writer(&mut wtr);
        for (sentiment, count) in &map {
            csv_writer.serialize(SentimentRecord { sentiment: sentiment.clone(), count: *count })
                .map_err(|_| ApiError::InternalError("Failed to write CSV".into()))?;
        }

        // Crucial step: Flush the writer
        csv_writer.flush().map_err(|_| ApiError::InternalError("Failed to flush CSV".into()))?;
    }

    // Now you can safely move the contents of 'wtr'
    let data = wtr;

    // 写回到 S3
    client.put_object()
        .bucket(bucket)
        .key(key)
        .body(ByteStream::from(data))
        .send()
        .await
        .map_err(|_| ApiError::S3Error)?;

    // 提取 positive 和 negative 的计数
    let positive_count = map.get("Positive").cloned().unwrap_or(0);
    let negative_count = map.get("Negative").cloned().unwrap_or(0);
    let total_count = positive_count + negative_count;

    println!("SentimentCount {{ total:{}, positive: {}, negative: {} }}", total_count, positive_count, negative_count);

    if total_count == 0 {
        return Ok((0.00, 0.00));
    }

    let positive_percentage = positive_count as f64 / total_count as f64 * 100.0;
    let negative_percentage = negative_count as f64 / total_count as f64 * 100.0;

    println!("SentimentPercent {{ positive: {}, negative: {} }}", positive_percentage, negative_percentage);

    let positive_percentage_rounded = (positive_percentage * 100.0).round() / 100.0;
    let negative_percentage_rounded = (negative_percentage * 100.0).round() / 100.0;

    let sum_rounded = positive_percentage_rounded + negative_percentage_rounded;

    let mut final_negative_percentage = negative_percentage_rounded;

    if sum_rounded > 100.0 {
        final_negative_percentage -= sum_rounded - 100.0;
    } else if sum_rounded < 100.0 {
        final_negative_percentage += 100.0 - sum_rounded;
    }

    println!("SentimentPercentRound {{ positive: {}, negative: {} }}", positive_percentage_rounded, final_negative_percentage);

    Ok((positive_percentage_rounded, final_negative_percentage))
}

async fn generate_sentiment_analyze(text: &str, sentiment: &Sentiment) -> Result<String, ApiError> {
    // get model
    let text_generation_model = unsafe {
        TEXT_GENERATION_MODEL.as_ref().unwrap().lock().await
    };

    let output_prefix = match sentiment.polarity {
        SentimentPolarity::Positive => "This text is considered sentiment positive because",
        SentimentPolarity::Negative => "This text is considered sentiment negative because",
    };
    // let output_prefix = "This text ";

    let description = match sentiment.polarity {
        SentimentPolarity::Positive => "positive",
        SentimentPolarity::Negative => "negative",
    };

    // let input_context = format!("Why the text is considered sentiment {}, please give a detail reason for the text: {}", description, text);
    let input_context = format!("Why the text: \"{}\" is considered sentiment {}, please give a detail reason for the text.", text, description);
    println!("input_context: '{}'", input_context);

    println!("output_prefix: {}", output_prefix);

    // let generated_texts = text_generation_model.generate(&[output_prefix], &*input_context);
    let generated_texts = text_generation_model.generate(&[output_prefix], &*input_context);

    // Print all generated texts
    println!("Generated texts:");
    for (index, gen_text) in generated_texts.iter().enumerate() {
        println!("Option {}: {:?}", index + 1, gen_text);
    }

    // Check if generated texts are not empty and return the first element
    if let Some(first_text) = generated_texts?.first() {
        // Find the last period in the text
        if let Some(last_period_index) = first_text.rfind('.') {
            // Return everything before the last period
            Ok(first_text[..last_period_index + 1].to_string())
        } else {
            // No period found, return the whole text or handle as needed
            Ok(first_text.clone())
        }
    } else {
        Err(ApiError::InternalError("No text generated".to_string()))
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[tokio::test]
//     async fn test_analyze_sentiment_and_update_s3() {
//         assert!(result.is_ok());
//     }
// }

