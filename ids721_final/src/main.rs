use axum::{routing::{get, post}, Router};
// use axum::Extension;
use tower_http::services::ServeDir;
// use std::sync::Arc;
// use tokio::sync::Mutex;
// use rust_bert::pipelines::sentiment::{SentimentModel, Sentiment};
use ids721_final::{initialize_models, function_handler};

//Root Route for Change Machine
async fn root() -> &'static str {
    "
    ids721_final

    **Primary Route:**
    /sentiment
    "
}

#[tokio::main]
async fn main() {
    initialize_models();

    let app = Router::new()
        .route("/", get(root))
        .route("/sentiment/analyze", post(function_handler))
        // .layer(Extension(sentiment_model))  // Add the sentiment model to the state
        .nest_service("/sentiment", ServeDir::new("sentiment"));

    // run our app with hyper, listening globally on port 3000
    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(listener, app).await.unwrap();
}
